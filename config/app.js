const db = require('./db');

// import tabela
db.student = require('../models/Student');
db.grupa = require('../models/Grupa'); 
db.vjezba = require('../models/Vjezba'); 
db.test = require('../models/Test'); 
db.greska = require('../models/Greska'); 

// veze izmedju modela/tabela
db.grupa.hasMany(db.student, {as: "studentiGrupe", foriegnKey: "grupa_id"});
// Model "grupa" sada ima metode: get/setStudentiGrupe
// Model "Student"...

// relacije izmedju modela "Vjezbe" i modula "Student" (N : M)
db.studentVjezba = db.student.belongsToMany(db.vjezba, { through: "studentVjezba", foreignKey: "student_id"})
db.vjezba.belongsToMany(db.student, {through: "studentVjezba", foreignKey: "vjezba_id"})

// relacije izmedju modela "Vjezbe" i njegovih "podmodela" "Test" i "Greska" (1 : N)
// (1 : N) - id jedne vjezbe ce se nalazi vise puta u ovim modelima
db.vjezba.hasMany(db.greska, {as: "greskeVjezbe", foriegnKey: "vjezba_id"});
db.vjezba.hasMany(db.test, {as: "testoviVjezbe", foriegnKey: "vjezba_id"});


db.sync()
.then(() => {
    console.log("Kreirane tabele u uneseni podaci!");
})

module.exports = db;