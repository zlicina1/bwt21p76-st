const { addStudent, editStudent, addLabs, editLabs, lengthStudents, clearLabs } = require('../model/modelDb.js');
const { ReadStudentCSV, ReadLabCSV } = require('../util/util.js');

 async function createStudent(req, res){
    const newStudent = req.body;
  //  console.log(newStudent)
    await addStudent(newStudent, true)
    .then((condition)=>{
        if (condition == false){
            res.writeHead(201, {'Content-Type': 'application/json'});
            res.end(JSON.stringify({status: "Kreiran student!"}));
        } else{
            res.writeHead(200, {'Content-Type': 'application/json'});
            var message = "Student sa indexom " + newStudent.index + " već postoji!";
            res.end(JSON.stringify({status: message}));
        }
    })
    .catch(err => console.log(err))
}

async function updateStudent(req, res, index){
    const groupStudent = req.body;
    await editStudent(groupStudent, index)
    .then((condition) => {
        if (condition){
            res.writeHead(200, {'Content-Type': 'application/json'});
            var message = "Promjenjena grupa studentu " + index;
            res.end(JSON.stringify({status: message}));
        } else{
            res.writeHead(500, {'Content-Type': 'application/json'});
            var message = "Student sa indexom " + index + " ne postoji";
            res.end(JSON.stringify({status: message}));
        }
    })
    .catch(err => console.log(err))
}
async function createStudents(req, res){
    const newStudents = ReadStudentCSV(req.body, false);   
    const size = newStudents.length;
    const failedAdds = [];
    var n = 0;
    for (var i = 0; i < size; i++){
        await addStudent(JSON.parse(newStudents[i]))
        .then((condition) => {
            if (!condition) n++
            else {
                var inArray = false;
                for (var j = 0; j < failedAdds.length; j++)
                    if (JSON.parse(newStudents[i]).index == failedAdds[j].index) {
                        inArray = true;
                        break;
                    }
                if (!inArray) failedAdds.push(JSON.parse(newStudents[i]));
            } 
        }).catch(err => console.log(err))
    }
    if (n == size){
        res.writeHead(201, {'Content-Type': 'application/json'});
        var message = "Dodano " + size + " studeneta!";
        res.end(JSON.stringify({status: message}));
    } else{
        res.writeHead(201, {'Content-Type': 'application/json'});
        var message = "Dodano " + n + " studeneta, a studenti ";
        for (var i = 0; i < failedAdds.length; i++){
            message += failedAdds[i].index;
            if (i != failedAdds.length - 1) message += ",";
        }
        message += " već postoje!";
        res.end(JSON.stringify({status: message}));  
    }
}

async function createLabs(req, res){
    await clearLabs().then(async(condition) =>{
        if (condition){
            const numberLabs = req.body.brojVjezbi;
            for (var i = 0; i < numberLabs; i++){
                await addLabs(i + 1);
            }
            res.writeHead(201, {'Content-Type': 'application/json'});
            var message = "Kreirano " + numberLabs + " vježbe/i!";
            res.end(JSON.stringify({status: message}));
        }
        else {
            res.writeHead(500, {'Content-Type': 'application/json'});
            var message = "Nema studenata!";
            res.end(JSON.stringify({status: message}));
        }
    })
}

function updateLabs(req, res, index, lab){
    var body = '';
    req.on('data', (data)=>{
        body += data;
    })
    req.on('end', async ()=>{
        const testDate = JSON.parse(body);
        var retObject = await editLabs(testDate, index, lab);
        if (!retObject.condition){
            res.writeHead(404, {'Content-Type': 'application/json'});
            var message = "Nije moguće ažužirati vježbe!";
            res.end(JSON.stringify({status: message}));
        } else {
            res.writeHead(201, {'Content-Type': 'application/json'});
            var object = {
                vjezba: lab,
                tacnost: retObject.succes,
                promjena: retObject.difference,
                greske: retObject.mistakes
            }
            res.end(JSON.stringify(object));
        }
    })
}

module.exports = {
    createStudent,
    updateStudent,
    createStudents,
    createLabs,
    updateLabs
}