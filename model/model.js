const fs = require('fs');
const { Module } = require('module');
const path = require('path');
// pomoćni modul sa dodatnim funkcijama
const { ReadStudentCSV, ReadLabCSV, 
        writeToDocumentStudent, writeToDocumentLab, arrayTests } = require('../util/util.js');
const { TestoviParser } = require('../util//testoviParser.js');
const { test } = require('mocha');

// putanje do fajlova
const pathToDate = path.join(__dirname, '../data/', 'studenti.csv');
const pathToDateLabs = path.join(__dirname, '../data/', 'vjezbe.csv');

// čitanje fajlova i pretvaranje u niz JSON objekata 
var Students, Labs;
var Dates = fs.readFileSync(pathToDate);
Students = ReadStudentCSV(Dates, true);   
Dates = fs.readFileSync(pathToDateLabs);
Labs = ReadLabCSV(Dates, true);
//
function ReadAgainFiles(){
    Dates = fs.readFileSync(pathToDate);
    Students = ReadStudentCSV(Dates, true);
    Dates = fs.readFileSync(pathToDate);
   // Labs = ReadLabCSV(Dates, true);
}
//----------------------------------------------------
// funkcije koje rade sa podacima(CSV)
//----------------------------------------------------
// dodavanje Studenta
function addStudent(newStudent, condition, i = 1){ 
    if (condition) ReadAgainFiles();
    else {
        if (i == 0) ReadAgainFiles();
    }
    const id = Students.findIndex((p)=> JSON.parse(p).index == newStudent.index);
    if (id == -1) { // ako nema indexa
        Students.push(JSON.stringify(newStudent));
       // console.log(Students);
        writeToDocumentStudent(Students);
        return false;
    }
    return true;
}
// editovanje Studenta
function editStudent(groupStudent, index){
    ReadAgainFiles();
    const id = Students.findIndex((p)=> JSON.parse(p).index == index);
    if (id == -1) { // ako nema indexa
        return false;
    }
    var copyStudents = Students;
    var Student = JSON.parse(copyStudents[id]);
    Student.grupa = groupStudent.grupa;
    copyStudents[id] = JSON.stringify(Student);
    Students = copyStudents;
    writeToDocumentStudent(Students);
    return true;
}
// dodavanje Vježbe
function addLabs(numberLabs){ 
    ReadAgainFiles();
    var labs = [];
    for (var i = 0; i < Students.length; i++){
        for (var j = 0; j < numberLabs; j++){
            var mistakes = []
            var tests = []
            var Lab = {
                index: JSON.parse(Students[i]).index,
                vjezba: j + 1,
                tacnost: 0 + "%",
                promjena: 0 + "%",
                greske: mistakes,
                testovi: tests
            }
        labs.push(Lab);
        }
    }
    writeToDocumentLab(labs);
}

// dodavanje Vježbe
async function editLabs(testDate, index, lab){ 
    Labs = await ReadLabCSV(Dates, true);
   // console.log(Labs);
    // ovaj objekat se vraća iz ove funkcije
    var retObject = {
        succes: "0%",
        difference: "0%",
        mistakes: [],
        condition: false
    }
    const idLab = Labs.findIndex((p)=> 
    JSON.parse(p).vjezba == lab && JSON.parse(p).index == index);
  //  console.log(idLab);
    if (idLab == -1) return retObject;
    if (JSON.parse(JSON.parse(Labs[idLab]).testovi).length == 0){
        const newUser = new TestoviParser();
        const obj = newUser.dajTacnost(JSON.stringify(testDate));
        retObject.succes = JSON.parse(obj).tacnost;
        retObject.mistakes = JSON.parse(obj).greske;
        retObject.condition = true;
        JSON.parse(Labs[idLab]).tacnost = JSON.parse(obj).tacnost;
        JSON.parse(Labs[idLab]).greske = JSON.parse(obj).greske;
        JSON.parse(Labs[idLab]).testovi = arrayTests(testDate);
        writeToDocumentLab(Labs);
        return retObject;
    }
    else {


        return retObject;
    }
}


module.exports = {
    addStudent,
    editStudent,
    addLabs,
    editLabs
}