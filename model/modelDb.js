const db = require('../config/db')

function addStudent(newStudent){
    return new Promise((resolve, reject) => {
        db.student.findOne({where:{index: newStudent.index}}).then(student => {
            if (student) resolve(true) 
            else {
                db.grupa.findOrCreate({where: {naziv: newStudent.grupa}})
                .then((grupa) => {
                    db.student.create(newStudent).then(noviStudent => {
                    // noviStudent = noviStudent[0];
                        grupa = grupa[0];
                        grupa.addStudentiGrupe(noviStudent);
                    })
                })
            }
            resolve(false)      
        }).catch(err => console.log(err))
    })
}

function editStudent(groupStudent, index){
    return new Promise((resolve, reject) => {
        db.student.findOne({where:{index: index}}).then(student => {
            if (student) {
                db.grupa.findOrCreate({where: {naziv: groupStudent.grupa}})
                .then((grupa) => {
                    grupa = grupa[0];
                    grupa.addStudentiGrupe(student);
                    resolve(true) 
                })
            } else resolve(false)  
        }).catch(err => console.log(err))
    })
}

function clearLabs(){
    return new Promise((resolve, reject) => {
        // dodatni parametri: radi foreignKey-ova koji su tabeli "studentVjezba"
        db.vjezba.destroy({truncate: {cascade: true}} ).then(()=>{
                resolve(true)
        }).catch(err => console.log(err))
    }).catch(err => console.log(err))
}

function addLabs(i){
    var newVjezba = {vjezba_id: i, tacnost: "0%", promjena: "0%"}
    return new Promise((resolve, reject) => {
        db.student.findAll().then((students) =>{
            if (students){
            students.forEach((elem)=>{
                db.vjezba.create(newVjezba).then((vjezba) => {
                    vjezba.addStudent(elem);
                }).catch(err => console.log(err))
                resolve(true)
            })
        }
        })
        resolve(false)
    }).catch(err => console.log(err))
}

/*
function addLabs(i){
    return new Promise((resolve, reject) => {
        var newVjezba = {tacnost: "0%", promjena: "0%"}
        db.vjezba.create(newVjezba).then((vjezba) => {
            db.student.findAll().then(students => {
                students.forEach(elem => {
                    vjezba.addStudent([vjezba])
                })
                resolve(vjezba)
            })
        })
    }).catch(err => console.log(err))
}
-*/
function lengthStudents(){
    return new Promise((resolve, reject) => {
        var size = db.student.count();
        resolve(size)
    })
}

module.exports = {
    addStudent,
    editStudent,
    lengthStudents,
    addLabs,
    clearLabs
}