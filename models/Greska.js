const Sequelize = require('sequelize');
const db = require('../config/db');

// ovaj modul se biti povezan sa modulom "Vjezba" kao 1 : n
const Greska = db.define("greska", {
    fulltitle: Sequelize.STRING,
    status: Sequelize.STRING
})

module.exports = Greska;