const Sequelize = require('sequelize');
const db = require('../config/db');

const Grupa = db.define("grupa", {
    //naziv: {type: Sequelize.STRING, unique: true}
    naziv: Sequelize.STRING
})

module.exports = Grupa;