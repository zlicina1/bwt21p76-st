const Sequelize = require('sequelize');
const db = require('../config/db');

const Student = db.define("student", {
    ime: Sequelize.STRING,
    prezime: Sequelize.STRING,
    index: Sequelize.STRING,
    //grupa_id: Sequelize.STRING
})

module.exports = Student;