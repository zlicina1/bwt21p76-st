const Sequelize = require('sequelize');
const db = require('../config/db');

const Vjezba = db.define("vjezba", {
    // polje "vjezba" sa spirale_2 se automatski kreira kao "id"
    vjezba_id: {
        type: Sequelize.INTEGER
    },
    tacnost: Sequelize.STRING,
    promjena: Sequelize.STRING
})

module.exports = Vjezba;