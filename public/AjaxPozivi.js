global.XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;
function fun(error, data){
    if (error != null) {
        var message = JSON.parse(error);
        document.getElementById('p').innerHTML = message.status;
        document.getElementById('p').style.color = "red";
    } 
    else {
        var message = JSON.parse(data);
        document.getElementById('p').innerHTML = message.status;
        document.getElementById('p').style.color = "green";
    } 
}

function buttonStudent(){
    var name = document.getElementById('ime').value;
    var surname = document.getElementById('prezime').value;
    var indeks = document.getElementById('index').value;
    var group = document.getElementById('grupa').value;
    var student = {
        ime: name, prezime: surname, index: indeks, grupa: group
    }
    posaljiStudent(student, fun);
}

function buttonGrupa(){
    var indeks = document.getElementById('index').value;
    var grupa = document.getElementById('grupa').value;
    postaviGrupu(indeks, grupa, fun);
}
function unosStudenataCSV(){
    var tekstCSV = document.getElementById('tekstCSV').value;
    //console.log(tekstCSV);
    posaljiStudente(tekstCSV, fun);
}
function kreirajVjezbe(){
    var broj = document.getElementById('brojVjezbi').value; 
    postaviVjezbe(broj, fun)
}

////////////
function posaljiStudent(studentObjekat, callback){
    //console.log(studentObjekat);
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = async ()=>{
        if (xhr.readyState == 4 && xhr.status == 200){
            var responseObject = xhr.responseText;
            callback(responseObject, null);
        }
        else if (xhr.readyState == 4 && xhr.status == 201){
            var responseObject = xhr.responseText;
            callback(null, responseObject);
        } 
    }
    xhr.open('POST', '/student', true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify(studentObjekat));
}

// @dec Update group
// @route POST /student/:index
function postaviGrupu(indexStudenta, grupa, callback){
   // console.log(indexStudenta + " - " + grupa); 
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = async ()=>{
        if (xhr.readyState == 4 && xhr.status == 500){
            var responseObject = xhr.responseText;
            callback(responseObject, null);
        }
        else if (xhr.readyState == 4 && xhr.status == 200){
            var responseObject = xhr.responseText;
            callback(null, responseObject);
        } 
    }
    var url = "/student/" + indexStudenta;
    xhr.open('POST', url, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify({grupa: grupa}));
}
// @dec Update group
// @route POST /student/:index
function posaljiStudente(studentiCSVString, callback){
    //console.log(studentiCSVString); 
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = async ()=>{
        if (xhr.readyState == 4 && xhr.status == 201){
            var responseObject = xhr.responseText;
            console.log(responseObject);
            callback(null, responseObject);
        }
        else if (xhr.readyState == 4 && xhr.status == 500){
            var responseObject = xhr.responseText;
            callback(responseObject, null);
        } 
    }
    var url = "/batch/student";
    xhr.open('POST', url, true);
   // xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(studentiCSVString);
}
// @dec Update group
// @route POST /student/:index
function postaviVjezbe(brojVjezbi, callback){
//    console.log(brojVjezbi); 
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = async ()=>{
        if (xhr.readyState == 4 && xhr.status == 201){
            var responseObject = xhr.responseText;
            callback(null, responseObject);
        }
        else if (xhr.readyState == 4 && xhr.status == 500){
            var responseObject = xhr.responseText;
            callback(responseObject, null);
        } 
    }
    var url = "/vjezbe";
    xhr.open('POST', url, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify({brojVjezbi: brojVjezbi}));
}

module.exports = {
    posaljiStudent,
    postaviGrupu,
    posaljiStudente,
    postaviVjezbe
}