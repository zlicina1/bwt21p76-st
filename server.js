const path = require('path');
const bodyParser = require('body-parser');
const express = require('express');
const db = require('./config/app')
//
const { createStudent, updateStudent, createStudents, createLabs, updateLabs } 
        = require('./controller/controller.js');
const app = express();
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.text())
//
app.post('/student', (req, res)=> {
    createStudent(req, res);
})
app.post('/student/:index', (req, res) => {
    var index = req.params.index;
    updateStudent(req, res, index)
})
app.put('/student/:index', (req, res) => {
    var index = req.params.index;
    updateStudent(req, res, index)
})
app.post('/batch/student', (req, res) => {
    createStudents(req, res);
})
app.post('/vjezbe', (req, res) =>{
    createLabs(req, res);
})

const PORT = 3000;
app.listen(3000, ()=>{
    console.log("Server running...");
})

module.exports = app;