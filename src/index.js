class TestoviParser{
    constructor(){

    }
    dajTacnost(str){
        const object = JSON.parse(str);
        var broj_testova = object.stats.tests;
        var broj_uspjesnih = object.stats.passes;
        var broj_neuspjesnih = object.stats.failures;
        var broj_neizvrsenih = object.stats.pending;
        var rez;
        var nizGresaka = [];
        if (broj_neizvrsenih == 0){
            rez = broj_uspjesnih / broj_testova;
            if (rez != 1){
                for (var j = 0; j < broj_neuspjesnih; j++){
                    nizGresaka[j] = object.failures[j].fullTitle;
                }
            }
        }
        else {
            rez = 0;
            nizGresaka += "Testovi se ne mogu izvršiti";
        }
        rez *= 100;
        rez = Math.round(rez * 10) / 10;
        var tacnost = rez + '%';
        var objekat = {
            "tacnost": tacnost,
            "greske": nizGresaka
        }
      //  console.log(JSON.stringify(objekat));
        return JSON.stringify(objekat);
    }
    porediRezultate(rez1, rez2){
        var obj1 = JSON.parse(rez1);
        var broj_testova1 = obj1.stats.tests;
        var broj_uspjesnih1 = obj1.stats.passes;
        var broj_neuspjesnih1 = obj1.stats.failures;
        var obj2 = JSON.parse(rez2);
        var broj_testova2 = obj2.stats.tests;
        var broj_uspjesnih2 = obj2.stats.passes;
        var broj_neuspjesnih2 = obj2.stats.failures;
        var isti_su = true;
        var brojac1 = 0, brojac2 = 0;
        var niz = [];
        if (broj_testova1 != broj_testova2){}
        else{
            for (var i = 0; i < broj_testova1; i++){
                var elem1 = obj1.tests[i];
                var j = 0;
                for (; j < broj_testova2; j++){
                    var elem2 = obj2.tests[j];
                    if (elem1.fullTitle == elem2.fullTitle) break;
                }
                if (j == broj_testova2) {
                    isti_su = false;
                    for (var k = 0; k < obj1.failures.length; k++){
                        if (elem1.fullTitle == obj1.failures[k].fullTitle){
                            niz[brojac1] = elem1;
                            brojac1++;
                            break;
                        }
                    }
                }
            }
        }
        console.log(isti_su + ' ' + brojac1);
        var x;
        var niz_gresaka = [];
        if (isti_su){
            var rez = (broj_uspjesnih2 / broj_testova2) * 100;
            x = Math.round(rez * 10) / 10;
            x = x + '%';
            niz_gresaka = obj1.failures;
        } else{    
            rez = (brojac1 + obj2.failures.length) / (brojac1 + obj2.tests.length) * 100;
            x = Math.round(rez * 10) / 10;
            x = x + '%';
            // greske koje se pojavljuju samo u rezultatu_2
            for (var i = 0; i < obj2.failures.length; i++){
                niz[brojac1] = obj2.failures[i];
                brojac1++;
            }
            niz_gresaka = niz;
        }
        var niz = [];
        for (var i = 0; i < niz_gresaka.length; i++)
            niz[i] = niz_gresaka[i].fullTitle;
        var Rez = {
            "promjena":x,
            "greske": niz
        }
        console.log(Rez);
        return JSON.stringify(Rez);
    }
};

module.exports = {
    TestoviParser
}
/*
const fs = require('fs');
const path = require('path');

console.log(path.join(__dirname, '../test', 'test2.json'));
const ts = new TestoviParser();
fs.readFile(path.join(__dirname, '../test', 'test2.json'), 'utf8', (err, data) =>{
    if (err) throw err;
    ts.dajTacnost(data);
});
*/
/*
<script src="https://unpkg.com/chai@4.3.4/chai.js"></script>
    <script src="https://unpkg.com/mocha@9.1.3/mocha.js"></script>

<div id="mocha"></div>

    <script id="module" class="mocha-init">
        mocha.checkLeaks();
        mocha.setup('bdd');
    </script>

        <script src="index.js"></script>
        <script src="../test/index.spec.js"></script>
    <script id="module">
        mocha.run();
    </script>  
    */