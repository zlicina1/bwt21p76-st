const { server } = require('../server.js');
const { util } = require('../util/util.js');
const fs = require('fs');
const path = require('path');

const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
const should = chai.should();

const pathToDate = path.join(__dirname, '../data/', 'studenti.csv')
describe('Test_POST: /student', ()=>{
    // očisti datoteku 'studenti.csv' prije svakog pokretanja ovog test-suita
    before((done)=>{
        fs.writeFile(pathToDate, 'ime,prezime,index,grupa\n', ()=>{});
        done();
    })
    // poslije ovog test-suita izbriši sve podatke iza datoteke
    after((done)=>{
        fs.writeFile(pathToDate, 'ime,prezime,index,grupa\n', ()=>{});
        done();
    })
       // objekt za pomoć pri testiranju odgovora
    const succesMessage = {
        status: "Kreiran student!"
    }
    //1
    it('Treba dodati jednog studenta', (done)=>{
        const newStudent = {
            ime: "Ime", prezime: "Prezime", index: "123", grupa: "RS1"
        }
        chai.request(server)
            .post('/student')
            .set('content-type', 'application/json')
            .send(JSON.stringify(newStudent))
            .end((err, res)=>{
                should.not.exist(err);  // ne smije biti gresaka
                res.should.have.status(201);    // status mora biti 201
                const resBody = res.body;   // body odgovora
               // console.log(resBody.status);
                resBody.status.should.be.eql(succesMessage.status);
                done();
            })
    })
    //2
    it('Ne treba dodati studenta jer taj index već postoji', (done)=>{
        const sameStudent = {
            ime: "DrugoIme", prezime: "DrugoPrezime", index: "123", grupa: "RS2"
        }
        const unSuccesMessage = {
            status: "Student sa indexom " + sameStudent.index + " već postoji!"
        }
        chai.request(server)
            .post('/student')
            .set('content-type', 'application/json')
            .send(JSON.stringify(sameStudent))
            .end((err, res)=>{
                should.not.exist(err);
                res.should.have.status(404);    // status mora biti 404
                const resBody = res.body;
                resBody.status.should.be.eql(unSuccesMessage.status); // poruka treba biti ista
                done();
            })
    })
    //3
    it('Treba dodati studenta jer index je različit, iako su ostala polja studenta ista', (done)=>{
        const newStudent = {
            ime: "Ime", prezime: "Prezime", index: "555", grupa: "RS1"
        }
        chai.request(server)
            .post('/student')
            .set('content-type', 'application/json')
            .send(JSON.stringify(newStudent))
            .end((err, res)=>{
                should.not.exist(err);
                res.should.have.status(201);   // status mora biti 201
                const resBody = res.body;
                resBody.status.should.be.eql(succesMessage.status); // poruka treba biti ista
                done();
            })
    })
})

