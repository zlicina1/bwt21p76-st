const { server } = require('../server.js');
const { writeToDocumentStudent, getCSVFormatStudent} = require('../util/util.js');
const fs = require('fs');
const path = require('path');

const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
const should = chai.should();
const pathToDate = path.join(__dirname, '../data/', 'studenti.csv')

describe('Test_POST: /batch/student', ()=>{
    // očisti datoteku 'studenti.csv' prije pokretanja svakog testa 
    // i ubaci podatke koje će nam poslužiti za testiranje (tj. ubaci studente)
    beforeEach((done)=>{
        const array = [
            {ime: "Zijad", prezime: "Ličina", index: "76", grupa:"RS1"},
            {ime: "Suljo", prezime: "Suljić", index: "100", grupa:"RS2"}
        ]
        var Students = [];
        for (var i = 0; i < array.length; i++)
            Students.push(JSON.stringify(array[i]));
        writeToDocumentStudent(Students);
        done();
    })
    // poslije ovog testa izbriši sve podatke iza datoteke
    afterEach((done)=>{
        fs.writeFile(pathToDate, 'ime,prezime,index,grupa\n', ()=>{});
        done();
    })
    // 1
    it('Trebaju se dodati svi studenti', (done)=>{
        const array = [
            {ime: "Novi", prezime: "Nović", index: "50", grupa:"RS1"},
            {ime: "Novi2", prezime: "Nović2", index: "51", grupa:"RS2"}
        ]
        var newStudents = [];
        for (var i = 0; i < array.length; i++)
            newStudents.push(JSON.stringify(array[i]));
        const bodyCSV = getCSVFormatStudent(newStudents, false);
        var message = "Dodano " + 2 + " studeneta!";
        chai.request(server)
            .post('/batch/student')
            .set('content-type', 'text/csv')
            .send(bodyCSV)
            .end((err, res)=>{
                should.not.exist(err);  // ne smije biti gresaka
                res.should.have.status(201);    // status mora biti 201
                const resBody = res.body;   // body odgovora
                resBody.status.should.be.eql(message);
                done();
            })
    })
    // 2
    it('Treba se dodati 1 student, jer student sa indexom: 100 već postoji', (done)=>{
        const array = [
            {ime: "Pero", prezime: "Perić", index: "100", grupa:"RS3"},
            {ime: "Novi", prezime: "Nović", index: "50", grupa:"RS1"}
        // {ime: "Novi2", prezime: "Nović2", index: "51", grupa:"RS2"},
        ]
        var newStudents = [];
        for (var i = 0; i < array.length; i++)
            newStudents.push(JSON.stringify(array[i]));
        const bodyCSV = getCSVFormatStudent(newStudents, false);
        var message = "Dodano " + 1 + " studeneta, a studenti " + 100 + " već postoje!";
        chai.request(server)
            .post('/batch/student')
            .set('content-type', 'text/csv')
            .send(bodyCSV)
            .end((err, res)=>{
                should.not.exist(err);  // ne smije biti gresaka
                res.should.have.status(201);    // status mora biti 201
                const resBody = res.body;   // body odgovora
                resBody.status.should.be.eql(message);
                done();
            })
    })
    // 3
    it('Ne treba se dodati nijedan student, jer indexi već postoje (duplo dodavanje istog indexa)', (done)=>{
        const array = [
            {ime: "Novo", prezime: "Nović", index: "76", grupa:"RS2"},
            {ime: "Pero", prezime: "Perić", index: "100", grupa:"RS3"},
            {ime: "Novo2", prezime: "Nović2", index: "100", grupa:"RS2"},
            {ime: "Pero2", prezime: "Perić2", index: "76", grupa:"RS5"},
            {ime: "Novo3", prezime: "Nović3", index: "100", grupa:"RS1"},
            {ime: "Novo4", prezime: "Nović4", index: "76", grupa:"RS5"},
        ]
        var newStudents = [];
        for (var i = 0; i < array.length; i++)
            newStudents.push(JSON.stringify(array[i]));
        const bodyCSV = getCSVFormatStudent(newStudents, false);
        var message = "Dodano " + 0 + " studeneta, a studenti 76,100 već postoje!";
        chai.request(server)
            .post('/batch/student')
            .set('content-type', 'text/csv')
            .send(bodyCSV)
            .end((err, res)=>{
                should.not.exist(err);  // ne smije biti gresaka
                res.should.have.status(201);    // status mora biti 201
                const resBody = res.body;   // body odgovora
                resBody.status.should.be.eql(message);
                done();
            })
    })
    //4
    // 3
    it('Trebaju se dodati 3 studenta, a 3 studenta se ne trebaju dodati', (done)=>{
        const array = [
            {ime: "Novo", prezime: "Nović", index: "3", grupa:"RS2"},
            {ime: "Pero", prezime: "Perić", index: "100", grupa:"RS3"},
            {ime: "Novo2", prezime: "Nović2", index: "1", grupa:"RS2"},
            {ime: "Pero2", prezime: "Perić2", index: "76", grupa:"RS5"},
            {ime: "Novo3", prezime: "Nović3", index: "100", grupa:"RS1"},
            {ime: "Novo4", prezime: "Nović4", index: "2", grupa:"RS5"},
        ]
        var newStudents = [];
        for (var i = 0; i < array.length; i++)
            newStudents.push(JSON.stringify(array[i]));
        const bodyCSV = getCSVFormatStudent(newStudents, false);
        var message = "Dodano " + 3 + " studeneta, a studenti 100,76 već postoje!";
        chai.request(server)
            .post('/batch/student')
            .set('content-type', 'text/csv')
            .send(bodyCSV)
            .end((err, res)=>{
                should.not.exist(err);  // ne smije biti gresaka
                res.should.have.status(201);    // status mora biti 201
                const resBody = res.body;   // body odgovora
                resBody.status.should.be.eql(message);
                done();
            })
    })
})