const chai = require('chai');
const path = require('path');
const assert = require('assert');
const sinon = require('sinon');
const AjaxPozivi = require('../../public/AjaxPozivi.js');
chai.should();
// package_test: "test": "mocha ./test/*.spec.js",

///------------------------------------------------
// Kreiramo niz "requests" u koji ćemo čuvati naše "lažne" zahtjeve 
// Ovi zahtjevi se kreiraju preko "Sinon" paketa, a lažni su zato što
// mi ne šaljemo ovaj zahtjev direktno na server, nego ga samo pravimo
var requests = [];
// pri svakom pozivu ajax zahtjevu, kreirat ćemo lažni zahtjev, 
// koji će imati iste atribute, kao i pravi zahtjev
XMLHttpRequest = function() {
    var fakeXhr = new sinon.FakeXMLHttpRequest();
    requests.push(fakeXhr);
    return fakeXhr;
}

describe('Testiranje metode "posaljiStudent"', ()=>{
    it ("Provjera ispravnosti uspješnog ajax poziva", (done) => {
        const student = {
            ime: "Novi", prezime: "Novic", index: "1", grupa: "RS1"
        }
        const studentJson = JSON.stringify(student);
        AjaxPozivi.posaljiStudent(student, (error, data)=>{});
        // provjera da li je prosao zahtjev
        assert.equal(requests.length, 1);    
        // provjera da li je url onakav kakav ocekujemo
        assert.equal(requests[0].url, '/student'); 
        // provjera da li je body ispravan
        assert.equal(requests[0].requestBody, studentJson);
         // da li je metoda 'POST'
         assert.equal(requests[8].method, 'POST');
        done();
    })
    it ("Provjera ispravnosti uspješnog ajax poziva(2)", (done) => {
        const student = {
            ime: null, prezime: null, index: null, grupa: null
        }
        const studentJson = JSON.stringify(student);
        AjaxPozivi.posaljiStudent(student, (error, data)=>{});
        // provjera da li je prosao zahtjev
        assert.equal(requests.length, 2);    
        // provjera da li je url onakav kakav ocekujemo
        assert.equal(requests[1].url, '/student'); 
        // provjera da li je body ispravan
        assert.equal(requests[1].requestBody, studentJson);
         // da li je metoda 'POST'
         assert.equal(requests[8].method, 'POST');
        done();
    })
    it ("Provjera ispravnosti uspješnog ajax poziva kada se pošalje JSON objekat", (done) => {
        const student = {
            ime: "Novi", prezime: "Novic", index: "1", grupa: "RS1"
        }
        const studentJson = JSON.stringify(student);
        AjaxPozivi.posaljiStudent(studentJson, (error, data)=>{});
        // provjera da li je prosao zahtjev
        assert.equal(requests.length, 3);    
        // provjera da li je url onakav kakav ocekujemo
        assert.equal(requests[2].url, '/student'); 
        // provjera da li je body ispravan
        assert.equal(requests[2].requestBody, JSON.stringify(studentJson));
         // da li je metoda 'POST'
         assert.equal(requests[8].method, 'POST');
        done();
    })
})

describe('Testiranje metode "postaviGrupu"', ()=>{
    it ("Provjera ispravnosti uspješnog ajax poziva", (done) => {
        const group = "RS1";
        const index = 1;
        const jsonGrupa = JSON.stringify({grupa: group});
        AjaxPozivi.postaviGrupu(index, group, (error, data)=>{});
        // provjera da li je prosao zahtjev
        assert.equal(requests.length, 4);    
        // provjera da li je url onakav kakav ocekujemo
        assert.equal(requests[3].url, '/student/' + index); 
        // provjera da li je body ispravan
        assert.equal(requests[3].requestBody, jsonGrupa);
         // da li je metoda 'POST'
         assert.equal(requests[8].method, 'POST');
        done();
    })
    it ("Provjera ispravnosti uspješnog ajax poziva(2)", (done) => {
        const group = null;
        const index = 55;
        const jsonGrupa = JSON.stringify({grupa: group});
        AjaxPozivi.postaviGrupu(index, group, (error, data)=>{});
        // provjera da li je prosao zahtjev
        assert.equal(requests.length, 5);    
        // provjera da li je url onakav kakav ocekujemo
        assert.equal(requests[4].url, '/student/' + index); 
        // provjera da li je body ispravan
        assert.equal(requests[4].requestBody, jsonGrupa);
         // da li je metoda 'POST'
         assert.equal(requests[8].method, 'POST');
        done();
    })
})
describe('Testiranje metode "posaljiStudente"', ()=>{
    it ("Provjera ispravnosti uspješnog ajax poziva", (done) => {
        const tekstCSV = "ime,prezime,1,grupa\nnovo,novic,2,grupa2";
        AjaxPozivi.posaljiStudente(tekstCSV, (error, data)=>{});
        // provjera da li je prosao zahtjev
        assert.equal(requests.length, 6);    
        // provjera da li je url onakav kakav ocekujemo
        assert.equal(requests[5].url, '/batch/student'); 
        // provjera da li je body ispravan
        assert.equal(requests[5].requestBody, tekstCSV);
         // da li je metoda 'POST'
         assert.equal(requests[8].method, 'POST');
        done();
    })
    it ("Provjera ispravnosti uspješnog ajax poziva", (done) => {
        const tekstCSV = "ime,prezime,1,grupa\nnovo,novic,2,grupa2";
        AjaxPozivi.posaljiStudente(tekstCSV, (error, data)=>{});
        // provjera da li je prosao zahtjev
        assert.equal(requests.length, 7);    
        // provjera da li je url onakav kakav ocekujemo
        assert.equal(requests[6].url, '/batch/student'); 
        // provjera da li je body ispravan
        assert.equal(requests[6].requestBody, tekstCSV);
         // da li je metoda 'POST'
         assert.equal(requests[8].method, 'POST');
        done();
    })
})

describe('Testiranje metode "postaviVjezbe"', ()=>{
    it ("Provjera ispravnosti uspješnog ajax poziva", (done) => {
        const broj = 5;
        const jsonVjezba = JSON.stringify({brojVjezbi: broj});
        AjaxPozivi.postaviVjezbe(broj, (error, data)=>{});
        // provjera da li je prosao zahtjev
        assert.equal(requests.length, 8);    
        // provjera da li je url onakav kakav ocekujemo
        assert.equal(requests[7].url, '/vjezbe'); 
        // provjera da li je body ispravan
        assert.equal(requests[7].requestBody, jsonVjezba);
         // da li je metoda 'POST'
         assert.equal(requests[8].method, 'POST');
        done();
    })
    it ("Provjera ispravnosti uspješnog ajax poziva(2)", (done) => {
        const broj = 2;
        const jsonVjezba = JSON.stringify({brojVjezbi: broj});
        AjaxPozivi.postaviVjezbe(broj, (error, data)=>{});
        // provjera da li je prosao zahtjev
        assert.equal(requests.length, 9);    
        // provjera da li je url onakav kakav ocekujemo
        assert.equal(requests[8].url, '/vjezbe'); 
        // provjera da li je body ispravan
        assert.equal(requests[8].requestBody, jsonVjezba);
        // da li je metoda 'POST'
        assert.equal(requests[8].method, 'POST');
        done();
    })
})
