const { server } = require('../server.js');
const { writeToDocumentStudent } = require('../util/util.js');
const fs = require('fs');
const path = require('path');

const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
const should = chai.should();
const pathToDate = path.join(__dirname, '../data/', 'studenti.csv')

describe('Test_PUT: /student/:index', ()=>{
    // očisti datoteku 'studenti.csv' prije svakog pokretanja ovog test-suita
    // i ubaci podatke koje će nam poslužiti za testiranje (tj. ubaci studente)
    before((done)=>{
        const array = [
            {ime: "Zijad", prezime: "Ličina", index: "76", grupa:"RS1"},
            {ime: "Suljo", prezime: "Suljić", index: "100", grupa:"RS2"},
            {ime: "Mujo", prezime: "Mujić", index: "50", grupa:"RS3"}
        ]
        var Students = [];
        for (var i = 0; i < array.length; i++)
            Students.push(JSON.stringify(array[i]));
        writeToDocumentStudent(Students);
        done();
    })
    // poslije ovog test-suita izbriši sve podatke iza datoteke
    after((done)=>{
        fs.writeFile(pathToDate, 'ime,prezime,index,grupa\n', ()=>{});
        done();
    })
    // objekti za pomoć pri testiranju odgovora
    var succesMessageObject = {
        status: "Promjenjena grupa studentu "
    }
    var unSuccesMessageObject = {
        status: "Student sa indexom "
    }
    // 1
    it('Treba promjeniti grupu studentu sa indexom: 100', (done)=>{
        const bodyReq = {
            "grupa": "RS10"
        }
        var indeks = 100;
        const message = succesMessageObject.status + indeks;
        chai.request(server)
            .put('/student/' + indeks)
            .set('content-type', 'application/json')
            .send(JSON.stringify(bodyReq))
            .end((err, res)=>{
                should.not.exist(err);  // ne smije biti gresaka
                res.should.have.status(201);    // status mora biti 201
                const resBody = res.body;   // body odgovora
               // console.log(resBody.status);
                resBody.status.should.be.eql(message);
                done();
            })
    })
    // 2
    it('Index: 120 ne postoji, grupa se ne mijenja', (done)=>{
        const bodyReq = {
            "grupa": "RS10"
        }
        var indeks = 120;
        const message = unSuccesMessageObject.status + indeks + " ne postoji";
        chai.request(server)
            .put('/student/' + indeks)
            .set('content-type', 'application/json')
            .send(JSON.stringify(bodyReq))
            .end((err, res)=>{
                should.not.exist(err);  // ne smije biti gresaka
                res.should.have.status(404);    // status mora biti 201
                const resBody = res.body;   // body odgovora
               // console.log(resBody.status);
                resBody.status.should.be.eql(message);
                done();
            })
    })
    // 3
    it('Treba ostati ista grupa, ako se pošalje ista grupa, i status treba biti uspješan', (done)=>{
        const bodyReq = {
            "grupa": "RS1"
        }
        var indeks = 76;
        const message = succesMessageObject.status + indeks;
        chai.request(server)
            .put('/student/' + indeks)
            .set('content-type', 'application/json')
            .send(JSON.stringify(bodyReq))
            .end((err, res)=>{
                should.not.exist(err);  // ne smije biti gresaka
                res.should.have.status(201);    // status mora biti 201
                const resBody = res.body;   // body odgovora
               // console.log(resBody.status);
                resBody.status.should.be.eql(message);
                done();
            })
    })
})