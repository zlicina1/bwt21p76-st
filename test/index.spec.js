const fs = require('fs');
const path = require('path');
const chai = require('chai');
const assert = chai.assert;

// Putanja do json datotetka - testova
const putanja = path.join(__dirname, '../test/resources');
var TestoviParser = require("../src/index.js");
const ts = new TestoviParser.TestoviParser;
//
    var nizRezultata = [];
//
describe("Zadatak_2:", ()=>{
    describe("Test_1:Treba da prolaze svi testovi", ()=>{
        it("Treba vratiti tačnost: 100% ako svi testovi prolaze, 'niz_gresaka' nema elemenata", ()=>{
            fs.readFile(path.join(putanja + '/test_1.json'), 'utf8', (err, data)=>{
                if(err) throw err;
                var rezultat = ts.dajTacnost(data);
                var obj = JSON.parse(rezultat);
                nizRezultata[nizRezultata.length] = rezultat;
                assert.equal("100%",obj.tacnost);
                assert.equal(0,obj.greske.length);
            });
        });
    });
    describe("Test_2:Treba da prolazi polovina testova, a polovina da ne prolazi", ()=>{
        it("Treba vratiti tačnost: 50% ako prolazi polovina testova, 'niz_gresaka' sadrži testove koji ne prolaze", ()=>{
            fs.readFile(path.join(putanja + '/test_2.json'), 'utf8', (err, data)=>{
                if(err) throw err;
                var rezultat = ts.dajTacnost(data);
                var obj = JSON.parse(rezultat);
                nizRezultata[nizRezultata.length] = rezultat;
                assert.equal("50%",obj.tacnost);
                assert.equal(3,obj.greske.length);
            });
        });
    });
    describe("Test_3:Treba da padaju svi testovi", ()=>{
        it("Treba vratiti tačnost: 0% ako padaju svi testovi, 'niz_gresaka' sadrži testove sve testove", ()=>{
            fs.readFile(path.join(putanja + '/test_3.json'), 'utf8', (err, data)=>{
                if(err) throw err;
                var rezultat = ts.dajTacnost(data);
                var obj = JSON.parse(rezultat);
                nizRezultata[nizRezultata.length] = rezultat;
                assert.equal("0%",obj.tacnost);
                assert.equal(3,obj.greske.length);
            });
        });
    });
    describe("Test_4:Treba da se testovi ne mogu izvršiti", ()=>{
        it("Treba vratiti tačnost: 0% ako se testovi ne mogu izvršiti, 'niz_gresaka' sadrži string koji sadrži poruku o tome", ()=>{
            fs.readFile(path.join(putanja + '/test_4.json'), 'utf8', (err, data)=>{
                if(err) throw err;
                var rezultat = ts.dajTacnost(data);
                var obj = JSON.parse(rezultat);
                nizRezultata[nizRezultata.length] = rezultat;
                assert.equal("0%",obj.tacnost);
                assert.equal("Testovi se ne mogu izvršiti",obj.greske);
            });
        });
    });
    describe("Test_5:Treba da samo jedan test prolazi", ()=>{
        it("Treba vratiti tačnost: 1/4% ako jedan test prolazi, 'niz_gresaka' sadrži sve testove koji ne prolaze", ()=>{
            fs.readFile(path.join(putanja + '/test_5.json'), 'utf8', (err, data)=>{
                if(err) throw err;
                var rezultat = ts.dajTacnost(data);
                var obj = JSON.parse(rezultat);                
                nizRezultata[nizRezultata.length] = rezultat;
                assert.equal(Math.round(1/4 * 1000) / 10 + "%",obj.tacnost);
                assert.equal(3,obj.greske.length);
            });
        });
    });
    describe("Test_6:Treba da prolazi polovina testova, a polovina da ne prolazi", ()=>{
        it("Treba vratiti tačnost: 50% ako prolazi polovina testova, 'niz_gresaka' sadrži testove koji ne prolaze", ()=>{
            fs.readFile(path.join(putanja + '/test_6.json'), 'utf8', (err, data)=>{
                if(err) throw err;
                var rezultat = ts.dajTacnost(data);
                var obj = JSON.parse(rezultat);
                nizRezultata[nizRezultata.length] = rezultat;
                assert.equal("50%",String(obj.tacnost));
                assert.equal(2,obj.greske.length);
            });
        });
       
    });
});

describe("Zadatak_4:", ()=>{
    describe("Test_1: Testovi identični", ()=>{
        it("Treba vratiti 'promjene': 50%(rez2), 'niz_gresaka' sadrži greške iz (rez1)", ()=>{
            fs.readFile(path.join(putanja + '/test_2.json'), 'utf8', (err1, data1)=>{
                if (err1) throw err1;
                fs.readFile(path.join(putanja + '/test_6.json'), 'utf8', (err2, data2)=>{
                    if (err2) throw err2;
                    var objRez1 = JSON.parse(nizRezultata[1]);
                    var objRez2 = JSON.parse(nizRezultata[5]);
                    var ukRez = JSON.parse(ts.porediRezultate(data1, data2));
                    assert.equal(ukRez.promjena, objRez2.tacnost);
                    assert.equal(ukRez.greske.length, objRez1.greske.length);
                    for (var i = 0; i < ukRez.greske.length; i++){
                        if(ukRez.greske[i] != objRez1.greske[i]){
                            assert.fail;
                        }
                    }
                });
            });
        });
    });
    describe("Test_2: Testovi identični", ()=>{
        it("Treba vratiti niz_gresaka bez elemenata, jer u rez_1 nisu padali testovi", ()=>{
            fs.readFile(path.join(putanja + '/test_1.json'), 'utf8', (err1, data1)=>{
                if (err1) throw err1;
                fs.readFile(path.join(putanja + '/test_5.json'), 'utf8', (err2, data2)=>{
                    if (err2) throw err2;
                    var objRez1 = JSON.parse(nizRezultata[0]);
                    var objRez2 = JSON.parse(nizRezultata[4]);
                    var ukRez = JSON.parse(ts.porediRezultate(data1, data2));
                    assert.equal(ukRez.promjena, objRez2.tacnost);
                    assert.equal(ukRez.greske.length, objRez1.greske.length);
                    for (var i = 0; i < ukRez.greske.length; i++){
                        if(ukRez.greske[i] != objRez1.greske[i]){
                            assert.fail;
                        }
                    }
                });
            });
        });
    });
});
