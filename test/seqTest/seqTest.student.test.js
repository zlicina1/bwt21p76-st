const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
const should = require('chai').should();
const path = require('path');
const assert = chai.assert;

const server = require(path.join(__dirname, '../../server.js'));
const db = require(path.join(__dirname, '../../config/app.js'));


describe("1) Test POST /student", ()=>{
    describe("Test POST /student", ()=>{
        it("Treba dodati studenta", (done)=>{
            var newStudent = {ime: "ime", prezime: "prezime", index: "t1", grupa: "r1"}
            chai.request(server)
                .post('/student')
                .set("content-type", "application/json")
                .send(newStudent)
                .end((err, res) => {
                    assert.equal(201, res.status)   // status treba biti 201
                    done()
                })
        })
    })
})
