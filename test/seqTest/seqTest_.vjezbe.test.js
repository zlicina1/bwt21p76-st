const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
const should = require('chai').should();
const path = require('path');
const assert = chai.assert;

const server = require(path.join(__dirname, '../../server.js'));
const db = require(path.join(__dirname, '../../config/app.js'));


describe("1) Test POST /vjezbe", ()=>{
    describe("Test POST /vjezbe", ()=>{
        it("Treba kreirati vjezbe", (done)=>{
            var broj = {brojVjezbi: 2}
            chai.request(server)
                .post('/vjezbe')
                .set("content-type", "application/json")
                .send(broj)
                .end((err, res) => {
                    assert.equal(201, res.status) // status treba biti 201
                    done()
                })
        })
    })
})
