const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
const should = require('chai').should();
const path = require('path');
const assert = chai.assert;

const server = require(path.join(__dirname, '../../server.js'));
const db = require(path.join(__dirname, '../../config/app.js'));


describe("1) Test PUT /student/:index", ()=>{
    describe("Test PUT /student", ()=>{
        it("Treba promjeniti grupu", (done)=>{
            var newStudent = {ime: "ime", prezime: "prezime", index: "t1", grupa: "r1"}
            var novaGrupa = {grupa: "r2"}
            chai.request(server)
                .put('/student/' + newStudent.index)
                .set("content-type", "application/json")
                .send(novaGrupa)
                .end((err, res) => {
                    assert.equal(200, res.status) // status treba biti 200
                    done()
                })
        })
    })
})
