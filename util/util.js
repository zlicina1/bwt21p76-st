const fs = require('fs');
const path = require('path');
const CSVToJSON = require('csvtojson');
const JSONToCSV = require('json-2-csv');

const pathToDate = path.join(__dirname, '../data/', 'studenti.csv')
const pathToDateLab = path.join(__dirname, '../data/', 'vjezbe.csv');

function ReadStudentCSV(date, condition){
  //  data = JSON.parse(date);
    var result = [];
    const rows = date.toString().split('\n');
    i = 1;
    if (condition) i = 1;
    else i = 0;
    for (; i < rows.length; i++){
        const strings = rows[i].split(',');
        if (strings.length == 4){
            var User = {
                ime: strings[0].toString(),
                prezime: strings[1].toString(),
                index: strings[2].toString(),
                grupa: strings[3].toString().trim("\r").trim(" ")
            }
            result.push(JSON.stringify(User));
        }
    }
    return result;
}

// korišten csv-to-json parser
async function ReadLabCSV(date, condition){
    var result;
    await CSVToJSON().fromFile(pathToDateLab)
    .then(dates=>{
        result = dates;
    })
    .catch((err)=>{
        if(err) throw err;
    })
    var resultfinal = [];
        for (var i = 0; i < result.length; i++)
            resultfinal.push(JSON.stringify(result[i]));
    return resultfinal;
}

function getCSVFormatStudent(Students, condition = true){
    var result = '';
    if (condition) result = 'ime,prezime,index,grupa\n';
    for (var i = 0; i < Students.length; i++){
        const Student = JSON.parse(Students[i]);
        var string = Student.ime.toString() + ',' + Student.prezime.toString() 
        + ',' + Student.index.toString() + ',' + Student.grupa.toString();
        result += string;
        if (i != Students.length - 1) result += '\n';
    }
    return result;
}
function writeToDocumentStudent(Students){
    const result = getCSVFormatStudent(Students);
    fs.writeFile(pathToDate, result, (err) => {
        if (err) throw err;
      //  console.log("Uspjesno upisani studenti!");
    })
}
function writeToDocumentLab(Labs){
   // console.log(Labs);
    JSONToCSV.json2csv(Labs, (csv, err)=>{
        if (err) throw err;
        fs.writeFile(pathToDateLab, csv, (err) => {
           // console.log(csv);
            if (err) throw err;
        })
    })
}

function arrayTests(TestDate){
    var result = [];
    for (i = 0; i < TestDate.tests.length; i++){
        result.push( TestDate.tests[i].fulltitle);
    }
    return result;
}

module.exports = {
    ReadStudentCSV,
    ReadLabCSV,
    writeToDocumentStudent,
    writeToDocumentLab,
    getCSVFormatStudent,
    arrayTests
}